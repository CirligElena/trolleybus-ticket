# Trolleybus ticket app
#### Description
Remember how in childhood we all checked trolleybus tickets and looked for lucky ones among them.
This program automates this process.
---
#### How to use:
After entering the ticket number in the text field and pressing the "search" button, the program will sum the first 3 numbers of the ticket and compare the result with the sum of the last three,
if the sum is equal, the message "Your ticket is lucky! Congratulations!" will appear on the screen, if not, a message will appear on how many tickets you need to buy to get the lucky one.

----
>MySQL - description:


>Each entered ticket is automatically stored in the MySQL database in a table "ticketstatistics" with the following colums:
>- "nr" 
>- "Ticket_number"
>- "Ticket_status"
>- "date_of_purchase"

![Screenshot_1](/TrolleybusTicket/img/Screenshot_1.png)

![Screenshot_2](/TrolleybusTicket/img/Screenshot_2.png)

![Screenshot_3](/TrolleybusTicket/img/Screenshot_3.png)
