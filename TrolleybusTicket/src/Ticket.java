
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

public class Ticket {
    JPanel windowContent = new JPanel();
    JLabel label1 = new JLabel("Introduce your ticket number");
    JTextField field1 = new JTextField(10);
    JButton go = new JButton("search");
    JTextArea result = new JTextArea();

    Ticket() throws SQLException, ClassNotFoundException {
        windowContent.add(label1);
        windowContent.add(field1);
        windowContent.add(go);
        windowContent.add(result);
        JFrame frame = new JFrame("Ticket App Ui");
        frame.setContentPane(windowContent);
        frame.setSize(500, 150);
        frame.setVisible(true);
        field1.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                if (field1.getText().length() >= 6) // limit textfield to 6 characters
                    e.consume();
            }
        });
        TicketAction ticketAction = new TicketAction(this);
        go.addActionListener(ticketAction);
    }
}



