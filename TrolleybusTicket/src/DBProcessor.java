import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;



public class DBProcessor {
    private Connection connection;

    public DBProcessor() throws ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    public Connection getConnection(String url, String username, String password) throws SQLException {
        if (connection != null)
            return connection;
        connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    public static void DBInsert(String number, String status) throws ClassNotFoundException, SQLException {
        final String USERNAME = "root";
        final String PASSWORD = "root";
        final String URL = "jdbc:mysql://localhost:3306/ticketsdb";
        DBProcessor db = new DBProcessor();
        Connection conn = db.getConnection(URL, USERNAME, PASSWORD);
        String insert = "insert into ticketstatistics (Ticket_number, ticket_status) values(?, ?)";
        PreparedStatement prepInsert = conn.prepareStatement(insert);
        prepInsert.setString(1, number);
        prepInsert.setString(2, status);
        prepInsert.execute();
    }
}
