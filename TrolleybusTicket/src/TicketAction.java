import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class TicketAction implements ActionListener {
    Ticket parent;

    TicketAction(Ticket parent) throws ClassNotFoundException, SQLException {
        this.parent = parent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextArea result = parent.result;
        String ticket_number = parent.field1.getText();

        try {
            int ticket = Integer.parseInt(ticket_number);
            //declaration of variables for the lucky ticket and counting how many tickets you should buy
            int countLT = 0;
            int luckyTicket = 0;

            if (parent.field1.getText().length() == 6) {
                //searching next lucky ticket
                while (ticket <= 999999) {
                    int n1 = ticket / 100000 % 10;
                    int n2 = ticket / 10000 % 10;
                    int n3 = ticket / 1000 % 10;
                    int n4 = ticket / 100 % 10;
                    int n5 = ticket / 10 % 10;
                    int n6 = ticket % 10;

                    if (n1 + n2 + n3 != n4 + n5 + n6) {
                        countLT++;
                        ticket++;
                    } else
                        break;

                    luckyTicket = ticket;
                }
                if (luckyTicket == 0) {
                    //printing out the result
                    result.setText("Your ticket is lucky! Congratulations!");
                    //Introduse the ticket to database ticketstatistics
                    DBProcessor.DBInsert(ticket_number, "Lucky");
                } else {
                    result.setText("Lucky ticket:" + luckyTicket + " will be after " + countLT + " tickets");
                    //Introduse the ticket to database ticketstatistics
                    DBProcessor.DBInsert(ticket_number, "Not Lucky");
                }
            } else if (parent.field1.getText().length() < 6)
                result.setText("Ticket number must contain 6 caracters from 000001 to 999999");
        } catch (NumberFormatException ex) {
            result.setText("This field must contain only numbers");
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}


